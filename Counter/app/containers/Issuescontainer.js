import React, {Component} from 'react';
import { Container, Content, List, ListItem, Text } from 'native-base';
import {StyleSheet, View, TouchableOpacity, TouchableHighlight} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../actions/counterActions';
import { connect } from 'react-redux';
import IssueHeader from '../components/Header/IssuesHeader';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';
import Issuewidget from '../components/Widget/Issues/IssuesWidgit';


class Issuescontainer extends Component {

    constructor(props) {
        super(props);
    }



    Issuescontainer(){
        this.props.navigator.push({
            id:'Issuescontainer'
        })
    }

    Guestfeedback(){
        this.props.navigator.push({
            id:'ReviewsFeed'
        })
    }

    openDrawer() {
        this.refs['myDrawer'].openDrawer();
    }

    render() {

        var navigationView = (

            <Container>
                <Content>

                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Menu </Text>
                    </View>

                    <List>
                        <ListItem >

                            <TouchableOpacity>
                                <Text>Guest Feedback</Text>
                            </TouchableOpacity>

                        </ListItem>
                        <ListItem>
                            <TouchableOpacity onPress={this.Guestfeedback.bind(this)}>
                                <Text>Reviews Feed</Text>
                            </TouchableOpacity>
                        </ListItem>

                        <ListItem>

                            <TouchableOpacity onPress={this.Issuescontainer.bind(this)}>
                                <Text> Issues </Text>
                            </TouchableOpacity>

                        </ListItem>

                    </List>
                </Content>
            </Container>

        );

        return (
            <DrawerLayoutAndroid
                ref="myDrawer"
                drawerWidth={300}
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={() => navigationView}>

                <IssueHeader opentheDrawer={this.openDrawer.bind(this)} />
                <Issuewidget/>


            </DrawerLayoutAndroid>
        );
    }
}


const styles = StyleSheet.create({

    FilterHeader:{
        backgroundColor:'skyblue',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,
    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:18,
    }

})

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(Issuescontainer);

