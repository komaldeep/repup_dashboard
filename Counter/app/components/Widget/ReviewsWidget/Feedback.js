import React, {Component} from 'react';
import { Container, Content, List, ListItem,CheckBox} from 'native-base';
import {StyleSheet,
    View,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Text,
    Picker,
    Item
     } from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/counterActions';
import { connect } from 'react-redux';
import ReviewsDetail from './ReviewsDetail';
import DrawerLayoutAndroid from 'DrawerLayoutAndroid';
import myTheme from '../../../Theme/Theme';
import PopupDialog from 'react-native-popup-dialog';
import Badge from 'react-native-smart-badge';

var feedbackjsonData = [{ Title:'AwesomeTrip', OverallRating:'9/10',
    OtpSource:'Expedia',Hotelname:'DemoHotel',Username:'Tom Kurchritz',
    UserCountry:'Poland',
    Date:'12/10/2016',
    UserComment:"Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    },

    { Title:'Like living there!!', OverallRating:'10/10',
        OtpSource:'Trip Adviser',Hotelname:'DemoHotel',Username:'Thomas',
        UserCountry:'Unite Kingdom',
        Date:'02/08/2016',
        UserComment:"Lorem Ipsum is simply dummy."
    },

    { Title:'Average', OverallRating:'8/10',
        OtpSource:'Expedia',Hotelname:'DemoHotel',Username:'Lana Milta',
        UserCountry:'Ukraine',
        Date:'12/10/2016',
        UserComment:""
    },

    { Title:'Average Hotel food was not good', OverallRating:'6.5/10',
        OtpSource:'HolidayIQ',Hotelname:'DemoHotel',Username:'Dennis Duncker',
        UserCountry:'Germany',
        Date:'15/06/2015',
        UserComment:"Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry. typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry"
    }]


var feedbackjsonData1 = { Title:'AwesomeTrip', OverallRating:'9/10',
    OtpSource:'Expedia',Hotelname:'DemoHotel',Username:'Tom Kurchritz',
    UserCountry:'Poland',
    Date:'12/10/2016',
    UserComment:"Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting  text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    Ratings:[{name:"overall-rating", rating:"4/5",}, {name:"Rooms", rating:"4/5",}, {name:"Frontdesk", rating:"4/5",}
        ,{name:"Service", rating:"4/5",},{name:"Staff Behaviour", rating:"4/5",},{name:"Food & Beverages", rating:"4/5",}]
}

class Feedback extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:'',
            optionSelected:'',
            ShowReply: false,
            clicked: false,
            selected1: 'key1',
            language:'',
        };
    }


    openDrawer() {
    this.refs['myDrawer'].openDrawer();
    }

    closeDrawer(){
        this.setState({
            clicked: true,
        })
    }


    openPopupBox(){
        this.popupDialog.openDialog();
    }

    render() {

        var navigationView = (

            <Container>
                <Content theme={myTheme}>

                    <View style={styles.FilterHeader}>
                        <Text style={styles.FilterHeaderstyle}> Filters </Text>
                    </View>


                    <List>
                        <ListItem>

                            <Picker
                                selectedValue={this.state.language}
                                onValueChange={(lang) => this.setState({language: lang})}>
                                <Picker.Item label="Februry 2017" value="Februry 2017" />
                                <Picker.Item label="January 2017" value="January 2017" />
                                <Picker.Item label="December 2016" value="December 2017" />
                                <Picker.Item label="November 2016" value="November 2017" />
                            </Picker>


                        </ListItem>


                        <ListItem itemDivider>
                            <Text> Properties </Text>
                        </ListItem>

                        <ListItem >
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text>Demo Hotel Gurgao</Text>
                        </ListItem>

                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text>Demo Hotel</Text>
                        </ListItem>

                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Demo Restauarnt </Text>
                        </ListItem>

                        <ListItem itemDivider>
                            <Text> OTA </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> All </Text>
                        </ListItem>
                        <ListItem>
                             <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Agoda </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Booking </Text>
                        </ListItem>
                        <ListItem>
                            <CheckBox selected={false} onPress={this.closeDrawer.bind(this)} />
                            <Text> Goibibo </Text>
                        </ListItem>

                    </List>

                </Content>
            </Container>
        );

        return (

            <DrawerLayoutAndroid
                                 ref="myDrawer"
                                 drawerWidth={240}
                                 drawerPosition={DrawerLayoutAndroid.positions.Right}
                                 renderNavigationView={() => navigationView}>

                <ScrollView>
                        <TouchableOpacity style={styles.Filterbar} onPress={this.openDrawer.bind(this)}>
                            <Text style={styles.Filter}>|  Filter </Text>
                        </TouchableOpacity>


                    {
                        feedbackjsonData.map((detail , i) => {
                            return (
                                <ReviewsDetail openpopupbox={this.openPopupBox.bind(this)} navigator={this.props.navigator} detail ={detail} key={i}/>
                            )
                        })
                    }


                </ScrollView>

                <PopupDialog
                    ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                    width= {320}
                    height={350}>

                    <ScrollView style={styles.alltags}>

                        {
                            feedbackjsonData1.Ratings.map((detail1 , i) => {
                                return (
                                    <ListItem>
                                        <Text style={styles.UserComment}> {detail1.name}-  </Text>
                                        <Badge style={styles.albadges} key={i}>
                                            {detail1.rating}
                                        </Badge>
                                    </ListItem>

                                )
                            })
                        }

                    </ScrollView>

                </PopupDialog>

            </DrawerLayoutAndroid>

        );
    }
}

const styles = StyleSheet.create({

    Filterbar:{
        marginRight: 10,
        marginBottom:2,
        alignItems:'flex-end',
    },

    Filter:{
        fontSize:15,
        color:"blue",
    },

    arrowstyle:{
        marginTop:14,
        marginLeft:8,
    },

    FilterHeader:{
        backgroundColor:'skyblue',
        alignItems:"center",
        paddingTop:10,
        paddingBottom:10,
    },

    FilterHeaderstyle:{
        color:"#ffffff",
        fontSize:18,
    },

    albadges:{
        backgroundColor:'rgba(102,178,255,1)',
    }


})



export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(Feedback);

