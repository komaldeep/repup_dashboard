import React, {Component} from 'react';
import {StyleSheet,
    ScrollView,
} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/counterActions';
import { connect } from 'react-redux';
import Issues from './Issues';

var feedbackjsonData = [{ Title:'AwesomeTrip', OverallRating:'9/10',
    OtpSource:'Expedia',Hotelname:'DemoHotel',Username:'Tom Kurchritz',
    UserCountry:'Poland',
    Date:'12/10/2016',
    UserComment:"Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting  text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    Ratings:[{name:"overall-rating", rating:"4/5",}, {name:"Rooms", rating:"4/5",}, {name:"Frontdesk", rating:"4/5",}
    ,{name:"Service", rating:"4/5",},{name:"Staff Behaviour", rating:"4/5",},{name:"Food & Beverages", rating:"4/5",}]
}]

class CreateIssueContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {

        };
    }


    render() {


        return (

                <ScrollView>

                    {
                        feedbackjsonData.map((detail , i) => {
                            return (
                                <Issues navigator={this.props.navigator} detail ={detail} key={i}/>
                            )
                        })
                    }

                </ScrollView>


        );
    }
}



export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(CreateIssueContainer);

