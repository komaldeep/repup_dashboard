import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../actions/counterActions';
import { connect } from 'react-redux';

const styles = StyleSheet.create({
    button: {
        width: 100,
        height: 30,
        padding: 10,
        backgroundColor: 'lightgray',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 3
    }
});

class Thirdpage extends Component {

    constructor(props) {
        super(props);
    }



    render() {

        return (

            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center',backgroundColor: 'blue' }}>
                <Text>
                    This is Third page
                </Text>
            </View>

        );
    }
}

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(Thirdpage);

