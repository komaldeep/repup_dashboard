import * as types from './actionTypes';

export function increment() {
  return {
    type: types.INCREMENT
  };
}

export function decrement() {
  return {
    type: types.DECREMENT
  };
}

export function Restdatafull() {

    return async function (dispatch) {
        try {
            let response = await
            fetch('https://facebook.github.io/react-native/movies.json');
            let responseJson = await
            response.json();
            console.log(responseJson.movies);
            dispatch ({
                type: "RESTDATA",
                payload: {
                    Restdata: responseJson.movies
                }
            })
        }
        catch (error) {
            console.error(error);
            dispatch ({
                type: "RESTDATA",
                payload: {
                    Restdata: error
                }
            })
        }
    }
}
