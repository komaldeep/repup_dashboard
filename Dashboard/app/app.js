import { Text, Navigator, TouchableHighlight } from 'react-native';
import React, {Component} from 'react';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import promise from "redux-promise-middleware";
import logger from "redux-logger";

import * as reducers from './reducers';
import ReviewsFeed from './containers/ReviewsFeed';
import CreateIssue from './containers/CreateIssue';
import Issuescontainer from './containers/Issuescontainer';
import GuestFeedback from './containers/GuestFeedback';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const reducer = combineReducers(reducers);
const store = createStoreWithMiddleware(reducer);


export default class App extends Component {


    navigatorRenderScene(route,navigator){

        _navigator = navigator;
        switch (route.id){
            case "ReviewsFeed":
                return(<ReviewsFeed navigator={navigator} title ="ReviewsFeed"/>)
            case "CreateIssue":
                return(<CreateIssue navigator = {navigator} title="CreateIssue"/> );
            case "Issuescontainer":
                return(<Issuescontainer navigator = {navigator} title="Issuescontainer"/>);
            case "GuestFeedback":
                return(<GuestFeedback navigator = {navigator} title="GuestFeedback"/>)

        }

    }

  render() {
    return (
          <Provider store={store}>

              <Navigator initialRoute={{ id: 'ReviewsFeed',  component: ReviewsFeed,
              }}
                         renderScene={this.navigatorRenderScene.bind(this)}/>

          </Provider>
    );
  }
}


