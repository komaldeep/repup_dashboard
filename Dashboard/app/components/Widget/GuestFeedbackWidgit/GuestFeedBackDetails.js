import React, {Component} from 'react';
import { Container, Content, Card, CardItem, Icon, Button, InputGroup, Input, ListItem, List } from 'native-base';
import {StyleSheet,
    View,
    Navigator,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Modal,
    Text} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/counterActions';
import { connect } from 'react-redux';
import myTheme from '../../../Theme/Theme';
import Badge from 'react-native-smart-badge';
import CreateIssue from './../../../containers/CreateIssue';
import PopupDialog from 'react-native-popup-dialog';

class GuestFeedBackDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:'',
            optionSelected:'',
            ShowReply: false,
            Count:'0',
        };
    }

    reply(){
        this.setState({
            ShowReply: true,
        });
    }

    createissue(){

        this.props.navigator.push({
            id:'CreateIssue'
        })

    }


    popupbox(){
        this.props.openpopupbox();
    }


    render() {

        if(this.state.ShowReply == true ){
            var ReplyBox =
                <View>
                    <InputGroup borderType='underline' >
                        <Input placeholder='Reply your Review Here' />
                    </InputGroup>
                </View>
        }
        else {
            var ReplyBox = <Text> </Text>
        }


        return (
            <Content theme={myTheme}>
                <Card style={styles.FeedbackRow} key={this.props.key}>

                    <CardItem>
                        <View style={styles.contentHeader}>
                            <View style={styles.ImageColumn}>
                                <Text style={styles.OverallRating}>
                                    {this.props.detail.OverallRating}
                                </Text>
                            </View>

                            <View style={styles.DetaisHeader}>

                                <Text style={styles.titleofotp}>
                                    {this.props.detail.Title}
                                </Text>

                                <View style={styles.nameandrating}>

                                    <Text style={styles.username}>
                                        <Icon style={styles.locationiconsize} name='md-contacts'/>   {this.props.detail.Username}  |  <Icon style={styles.locationiconsize} name='ios-pin'/> {this.props.detail.UserCountry}
                                    </Text>

                                </View>

                                <Text style={styles.username}>
                                    {this.props.detail.Date}
                                </Text>

                            </View>

                        </View>

                        <View style={styles.Listfooter}>

                            <List>
                            {
                                this.props.detail.Ratings.map((detail , i) => {
                                    return (
                                        <ListItem>
                                            <View style={styles.Listviewtable}>
                                                <Text style={styles.listviewfontheader}> {detail.name}- </Text>
                                                <Text style={styles.listviewfont}> {detail.rating} </Text>
                                            </View>
                                        </ListItem>
                                    )
                                })
                            }

                            </List>

                        </View>

                        <View>
                            <Text style={styles.UserComment}>
                                {this.props.detail.UserComment}
                            </Text>
                        </View>

                        <View style={styles.buttonsfooter}>

                            <Button
                                onPress={this.reply.bind(this)}
                                transparent
                                style={styles.buttonread}
                                textStyle={styles.buttontextstyle}
                            >
                                <Icon style={styles.locationiconsize} name='ios-chatbubbles' />
                                <Text>  Reply the Review</Text>
                            </Button>

                            <Button transparent
                                    style={styles.buttonread}
                                    textStyle={styles.buttontextstyle}>
                                <Icon style={styles.locationiconsize} name='ios-pricetag' />
                                <Text> Create New Issue </Text>
                            </Button>

                        </View>
                        {ReplyBox}
                    </CardItem>
                </Card>

            </Content>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(211,211,211,0.9)',
        paddingTop: 20,
    },

    FeedbackRow:{
        flex:1,
        marginLeft:7,
        marginRight:7,
    },

    contentHeader:{
        flex:1,
        flexDirection:'row',
        marginBottom:10,
    },

    ImageColumn:{
        width: 45,
        height: 45,
        backgroundColor:'rgba(0,128,0,0.5)',
        alignItems:"center",
        justifyContent:"center",
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
    },

    titleofotp:{
        fontSize: 14,
        marginLeft: 15,
        fontWeight:"bold",
    },

    DetaisHeader:{
        flex: 1,
        flexDirection: 'column',
    },

    username:{
        fontSize:11,
        marginLeft: 15,
        marginTop:5,
    },

    RatingCards:{
        flex: 1,
        flexDirection: 'row',
    },

    OverallRating:{
        color:'#fff',
        fontWeight: 'bold',
        fontSize:11,
    },

    UserComment:{
        opacity:0.9,
        fontSize: 12,
    },

    buttonsfooter:{
        flex:1,
        flexDirection:'row',
        marginTop:10,
        backgroundColor: 'rgba(211,211,211,0.3)',
        height:22,
        paddingBottom:6,
    },

    buttonread:{
        flex:1,
        // fontSize:7,
        alignSelf:"center",
        opacity:0.4,
        justifyContent: 'center'
    },

    Hotel:{
        fontSize:12,
        textAlign: "right",
        fontWeight:'bold',
        color:'blue',
        opacity:0.7,
    },

    texttags:{
        color:'blue',
    },

    buttontextstyle:{
        color:"rgb(0,128,0)",
        fontSize:11,
    },

    locationiconsize:{
        fontSize:15,
        color:"blue"
    },

    Listfooter:{
        // marginTop:10,
        backgroundColor: 'rgba(211,211,211,0.11)',
        paddingBottom:6,
        marginBottom:5,
    },

    listviewfontheader:{
    fontWeight:'bold',
        fontSize:11,
        opacity:0.9
    },

    listviewfont:{
         fontSize:12,
     },

    Listviewtable:{
       flexDirection:'row',
    },


});

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(GuestFeedBackDetails);



