import React, {Component} from 'react';
import { Container, Content, Card, CardItem, Icon, Button, InputGroup, Input, List, ListItem,} from 'native-base';
import {StyleSheet,
    View,
    Navigator,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Modal,
    Picker,
    Item,
    Text} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/counterActions';
import { connect } from 'react-redux';
import myTheme from '../../../Theme/Theme';
import Badge from 'react-native-smart-badge';
import CreateIssue from './../../../containers/CreateIssue';

class Issues extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:'',
            optionSelected:'',
            ShowReply: false,
            Count:'0',
            language:'',
        };
    }

    reply(){
        this.setState({
            ShowReply: true,
        });
    }

    createissue(){
        this.props.navigator.push({
            id:'CreateIssue'
        })
    }

    render() {
        return (
            <Content theme={myTheme}>
                <Card style={styles.FeedbackRow} key={this.props.key}>

                    <CardItem>
                        <View style={styles.contentHeader}>
                            <View style={styles.ImageColumn}>
                                <Text style={styles.OverallRating}>
                                    {this.props.detail.OverallRating}
                                </Text>
                            </View>

                            <View style={styles.DetaisHeader}>

                                <Text style={styles.titleofotp}>
                                    {this.props.detail.Title}
                                </Text>

                                <View>

                                    <Text style={styles.username}>
                                        {this.props.detail.Username} |  {this.props.detail.UserCountry}
                                    </Text>

                                </View>

                                <Text style={styles.username}>
                                    {this.props.detail.Date}
                                </Text>

                            </View>

                            <View>
                                <Text>
                                {this.props.detail.OtpSource}
                                </Text>
                            </View>
                        </View>


                        <View>

                            <Text style={styles.UserComment}>
                                {this.props.detail.UserComment}
                            </Text>

                        </View>

                        <View style={styles.createissuesview}>

                          <InputGroup borderType='underline'>
                              <Input placeholder='Tags'/>
                          </InputGroup>


                            <Picker
                              selectedValue={this.state.language}
                              onValueChange={(lang) => this.setState({language: lang})} style={styles.pickerstyle}>
                                <Picker.Item label="Assigned To-" />
                                <Picker.Item label="Front Desk" value="Front Desk" />
                                <Picker.Item label="Demo Account" value="Demo Account" />
                                <Picker.Item label="Komaldeep" value="Komaldeep" />
                            </Picker>


                            <InputGroup borderType='underline' style={styles.inputstyle}>
                                <Input placeholder='Keep In CC (Use it to send Email)'/>
                            </InputGroup>

                            <Button block > Submit </Button>

                        </View>

                    </CardItem>
                </Card>
            </Content>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(211,211,211,0.9)',
        paddingTop: 20,
    },

    FeedbackRow:{
        flex:1,
        marginBottom:20,
        paddingBottom:15,
        marginLeft:7,
        marginRight:7,
        marginTop:20,
        paddingTop:5,
    },

    contentHeader:{
        flex:1,
        flexDirection:'row',
        marginBottom:10,
    },

    ImageColumn:{
        width: 45,
        height: 45,
        backgroundColor:'rgba(0,128,0,0.5)',
        alignItems:"center",
        justifyContent:"center",
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
    },

    titleofotp:{
        fontSize: 14,
        marginLeft: 15,
        fontWeight:"bold",
    },

    DetaisHeader:{
        flex: 1,
        flexDirection: 'column',
    },

    username:{
        fontSize:11,
        marginLeft: 15,
        marginTop:5,
    },

    RatingCards:{
        flex: 1,
        flexDirection: 'row',
    },

    OverallRating:{
        color:'#fff',
        fontWeight: 'bold',
        fontSize:12,
    },

    UserComment:{
        opacity:0.9,
        fontSize: 12,
        marginBottom:9,
        paddingLeft:9,
    },

    buttonsfooter:{
        flex:1,
        flexDirection:'row',
        marginTop:10,
        alignItems:"center",
        justifyContent:"center",
    },

    buttonread:{
        flex:1,
    },

    Hotel:{
        fontSize:12,
        textAlign: "right",
        fontWeight:'bold',
        color:'blue',
        opacity:0.7,
    },

    buttonstyle:{
        marginTop:13,
    },

    alltags:{
        // flexDirection:'row',
        flexWrap:"wrap",
        alignItems:'center',
    },

    Discuss:{
        // flexDirection:'row',
        flexWrap:"wrap",
        alignItems:'center',
        paddingLeft:20,
        paddingRight:20,
    },

    insidetags:{
        flexDirection:'row',
        flexWrap:"wrap",
    },

    pickerstyle:{

    },

    createissuesview:{
        marginLeft:9,
        marginRight:15,
    },

    inputstyle:{
        marginBottom:20,
    }
});

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(Issues);



