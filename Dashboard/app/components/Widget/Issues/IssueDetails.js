import React, {Component} from 'react';
import { Container, Content, Card, CardItem, Icon, Button, InputGroup, Input, List, ListItem, } from 'native-base';
import {StyleSheet,
    View,
    Navigator,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    ScrollView,
    Alert,
    Modal,
    Text} from 'react-native';
import {bindActionCreators} from 'redux';
import * as counterActions from '../../../actions/counterActions';
import { connect } from 'react-redux';
import myTheme from '../../../Theme/Theme';
import Badge from 'react-native-smart-badge';
import CreateIssue from './../../../containers/CreateIssue';
import PopupDialog from 'react-native-popup-dialog';

class IssueDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible:true,
            value: 0,
            selectedOption:'',
            optionSelected:'',
            ShowReply: false,
            Count:'0',
        };
    }

    reply(){
        this.setState({
            ShowReply: true,
        });
    }

    createissue(){
        this.props.navigator.push({
            id:'CreateIssue'
        })
    }

    opentagspopup(){
        this.props.openpopupbox();
    }

    OpenDiscuustab(){

        this.props.Discussopenpopupbox();
    }


    render() {

        if(this.state.ShowReply == true ){
            var ReplyBox =
                <View>
                    <InputGroup borderType='underline' >
                        <Input placeholder='Reply your Review Here' />
                    </InputGroup>
                </View>
        }
        else {
            var ReplyBox = <Text> </Text>
        }


        return (
            <Content theme={myTheme}>
                <Card style={styles.FeedbackRow} key={this.props.key}>

                    <CardItem>
                        <View style={styles.contentHeader}>
                            <View style={styles.ImageColumn}>
                                <Text style={styles.OverallRating}>
                                    {this.props.detail.OverallRating}
                                </Text>
                            </View>

                            <View style={styles.DetaisHeader}>

                                <Text style={styles.titleofotp}>
                                    {this.props.detail.Title}
                                </Text>

                                <View style={styles.nameandrating}>
                                    <Text style={styles.username}>
                                        <Icon style={styles.locationiconsize} name='md-contacts'/>  {this.props.detail.Username}  |  <Icon style={styles.locationiconsize} name='ios-pin'/> {this.props.detail.UserCountry}
                                    </Text>
                                </View>

                                <Text style={styles.username}>
                                    {this.props.detail.Date}
                                </Text>
                            </View>

                            <View>
                                <Text>
                                    {this.props.detail.OtpSource}
                                </Text>
                            </View>
                        </View>


                        <View>
                            <Text style={styles.UserComment}>
                                {this.props.detail.UserComment}
                            </Text>
                        </View>


                        <View style={styles.Listfooter}>
                        <List>
                            <ListItem>

                                <View style={styles.Listviewtable}>
                                     <Text style={styles.listviewfontheader}> Property- </Text>
                                    <Text style={styles.listviewfont}> Demo Hotel Gurgao</Text>
                                </View>

                            </ListItem>
                            <ListItem>

                                <View style={styles.Listviewtable}>
                                    <Text style={styles.listviewfontheader}> Created- </Text>
                                    <Text style={styles.listviewfont}> 11/07/2015</Text>
                                </View>

                            </ListItem>
                            <ListItem>

                                <View style={styles.Listviewtable}>
                                    <Text style={styles.listviewfontheader}> Status- </Text>
                                    <Text style={styles.listviewfont}> Open </Text>
                                </View>

                            </ListItem>
                            <ListItem>

                                <View style={styles.Listviewtable}>
                                    <Text style={styles.listviewfontheader}> Assigned To </Text>
                                    <Text style={styles.listviewfont}> Komaldeep</Text>
                                </View>

                            </ListItem>
                        </List>
                        </View>



                        <View style={styles.buttonsfooter}>

                            <Button
                                onPress={this.opentagspopup.bind(this)}
                                transparent style={styles.buttonread}
                                textStyle={styles.buttontextstyle}>
                                <Icon style={styles.locationiconsize} name='md-folder-open' />
                                <Text> Rating-Tags </Text>
                            </Button>


                            <Button
                                transparent
                                style={styles.buttonread}
                                textStyle={styles.buttontextstyle}>
                                <Icon style={styles.locationiconsize} name='md-close' />
                                <Text>Close Issue</Text>
                            </Button>

                            <Button transparent
                                    onPress={this.OpenDiscuustab.bind(this)}
                                    style={styles.buttonread}
                                    textStyle={styles.buttontextstyle}>

                                <Icon style={styles.locationiconsize} name='ios-chatboxes' />
                                <Text> Discuss </Text>
                            </Button>

                        </View>

                    </CardItem>
                </Card>

            </Content>




        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(211,211,211,0.1)',
        paddingTop: 20,
    },

    FeedbackRow:{
        flex:1,
        marginLeft:7,
        marginRight:7,
    },

    contentHeader:{
        flex:1,
        flexDirection:'row',
        marginBottom:10,
    },

    ImageColumn:{
        width: 45,
        height: 45,
        backgroundColor:'rgba(0,128,0,0.5)',
        alignItems:"center",
        justifyContent:"center",
        borderTopRightRadius:5,
        borderTopLeftRadius:5,
        borderBottomRightRadius:5,
        borderBottomLeftRadius:5,
    },

    titleofotp:{
        fontSize: 14,
        marginLeft: 15,
        fontWeight:"bold",
    },

    DetaisHeader:{
        flex: 1,
        flexDirection: 'column',
    },

    username:{
        fontSize:11,
        marginLeft: 15,
        marginTop:5,
    },

    RatingCards:{
        flex: 1,
        flexDirection: 'row',
    },

    OverallRating:{
        color:'#fff',
        fontWeight: 'bold',
        fontSize:11,
    },

    UserComment:{
        opacity:0.9,
        fontSize: 12,
    },

    Listfooter:{
        marginTop:10,
        backgroundColor: 'rgba(211,211,211,0.11)',
        paddingBottom:6,
    },

    buttonsfooter:{
        flex:1,
        flexDirection:'row',
        marginTop:10,
        alignItems:"center",
        justifyContent:"center",
        backgroundColor: 'rgba(211,211,211,0.4)',
        height:22,
        paddingBottom:6,
    },

    buttonread:{
        flex:0.3,
        // fontSize:7,
        alignSelf:"center",
        opacity:0.4
    },

    Hotel:{
        fontSize:12,
        textAlign: "right",
        fontWeight:'bold',
        color:'blue',
        opacity:0.7,
    },

    texttags:{
        color:'blue',
    },

    buttontextstyle:{
        color:"rgb(0,128,0)",
        fontSize:11,
    },

    listviewfontheader:{
        fontWeight:'bold',
        fontSize:11,
        opacity:0.9
    },

    listviewfont:{
        fontSize:12,
    },

    Listviewtable:{
        flexDirection:'row',
    },

    locationiconsize:{
        fontSize:15,
        color:"blue",
    }

});

export default connect(state => ({
        state: state.counter,
    }),
    (dispatch) => ({
        actions: bindActionCreators(counterActions, dispatch)
    })
)(IssueDetails);



