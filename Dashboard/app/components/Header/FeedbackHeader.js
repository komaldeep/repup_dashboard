import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, TouchableHighlight} from 'react-native';
import { Container, Header, Title, Button, Icon } from 'native-base';

export default class FeedbackHeader extends Component {

    constructor(props) {
        super(props);
    }


    DrawerOpen(){
        this.props.opentheDrawer();
    }


    render() {
        return (
                <Header>

                    <Button transparent onPress={this.DrawerOpen.bind(this)}>
                        <Icon name="ios-menu" />
                    </Button>

                    <Title>
                        Reviews Feed
                    </Title>

                </Header>
        );
    }
}




